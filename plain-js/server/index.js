const PROTO_PATH = __dirname + '/../../proto/users.proto';

const findOneById = require('./server')

const grpc = require('@grpc/grpc-js');
const protoLoader = require('@grpc/proto-loader');
const packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    });
const users_proto = grpc.loadPackageDefinition(packageDefinition).users;

function main_server() {
    const server = new grpc.Server();

    server.addService(users_proto.UsersService.service, {
        findOneById
    });

    server.bindAsync('localhost:7777', grpc.ServerCredentials.createInsecure(), () => {
        server.start();
    });
}

main_server();
