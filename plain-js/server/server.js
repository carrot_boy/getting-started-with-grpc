function findOneById(call, callback) {
    console.log(`User with id ${call.request.user_id} is received`)

    callback(null, {
        id: call.request.user_id,
        name: "John Doe",
        city: "Berlin",
        age: 18
    });
}

module.exports = findOneById
