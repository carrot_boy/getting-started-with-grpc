const PROTO_PATH = __dirname + '/../../proto/users.proto';

const grpc = require('@grpc/grpc-js');
const protoLoader = require('@grpc/proto-loader');
const packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    });
const users_proto = grpc.loadPackageDefinition(packageDefinition).users;

function main_client() {
    const credentials = grpc.credentials.createInsecure();
    const client = new users_proto.UsersService('localhost:7777', credentials);

    client.FindOneById({user_id: 123}, function (err, response) {
        console.log('User:', response);
    });
}

main_client();
