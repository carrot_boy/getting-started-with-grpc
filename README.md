# Getting Started with gRPC

It is my attempt to implement a simple server and client with gRPC communication.

## The easiest way to run (with javascript):
- Go to the `plain-js` folder
- Install all dependencies (`yarn` or `npm i`)
- In the first terminal run `server`: `node ./server/index.js`
- Open the second terminal and run the `client`: `node ./client/index.js`
- See the response in the second terminal and log in to the first one
- _Optional_: run tests with `npm run tests` or `yarn tests`. Or run it directly `./node_modules/.bin/jest`

## The run with the pre-compiling libraries (with Golang):
- Go to the `go` folder
- Compile the server and client libraries: `./proto.sh` (run the script with `bash`)
> The script requires a `protoc` [compiler](https://github.com/protocolbuffers/protobuf#protobuf-runtime-installation) and [plugin](https://grpc.io/docs/languages/go/quickstart/#prerequisites) for Go language.
- Install go modules: `go mod tidy`
- In the first terminal run `server`: `go run ./server/main.go`
- Open the second terminal and run the `client`: `go run ./client/main.go`
- See the response in the second terminal and log in to the first one
- _Optional_: run tests with `go test -v ./server/`

## Cross-running:
- You can run either `plain-js` server and `go` client or vice versa
