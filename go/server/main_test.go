package main

import (
	"fmt"
	users "getting-started-with-grpc/dist"
	"golang.org/x/net/context"
	"testing"
)

type FindByIdTestCase struct {
	in users.UserById
	expected users.User
}

func TestFindById(t *testing.T)  {
	testContext := context.TODO()
	server := &Server{}

	for _, tt := range findByIdDataProvider() {
		testName := fmt.Sprintf("%d", tt.in.UserId)
		t.Run(testName, func(t *testing.T) {
			actual, _ := server.FindOneById(testContext, &tt.in)
			if actual.Id != tt.expected.Id {
				t.Errorf("got %v, want %v", actual, &tt.expected)
			}
		})
	}
}

func findByIdDataProvider() []FindByIdTestCase {
	testUserId := uint32(111)
	simpleCase := FindByIdTestCase{
		users.UserById{
			UserId: testUserId,
		},
		users.User{
			Id: testUserId,
		},
	}

	return []FindByIdTestCase{
		simpleCase,
	}
}
