package main

import (
	users "getting-started-with-grpc/dist"
	"log"
	"net"

	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

type Server struct {
}

func (s *Server) FindOneById(ctx context.Context, in *users.UserById) (*users.User, error) {
	log.Printf("Receive name: %d", in.UserId)

	return &users.User{
		Id:   in.UserId,
		Name: "Name",
		Age:  18,
		City: "Berlin",
	}, nil
}

func main() {
	lis, err := net.Listen("tcp", "localhost:7777")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	userServer := &Server{}
	grpcServer := grpc.NewServer()
	users.RegisterUsersServiceServer(grpcServer, userServer)
	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %s", err)
	}
}
