package main

import (
	users "getting-started-with-grpc/dist"
	"log"

	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

func main() {
	conn, err := grpc.Dial("localhost:7777", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Could not connect: %s", err)
	}
	defer conn.Close()

	c := users.NewUsersServiceClient(conn)
	request := &users.UserById{UserId: 123}
	response, err := c.FindOneById(context.Background(), request)
	if err != nil {
		log.Fatalf("Error when calling FindOneById: %s", err)
	}

	log.Printf("Response from server: %v", response)
}
